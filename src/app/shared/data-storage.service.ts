import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

//import { AuthService } from '../auth/auth.service';
import { DocumentService } from '../documents/documents.service';
import { Document } from '../documents/documents.model';
import { DocumentConfig } from '../documents/document-config/document-config.model';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(
    private http: HttpClient,
    private documentService: DocumentService,
  ) {}

  storeDocumentConfig() {
    const documentConfig = this.documentService.getDocumentConfig();
    this.http
      .put(
        'https://notepad-a952b-default-rtdb.firebaseio.com/document-config.json',
        documentConfig
      )
      .subscribe(response => {
        console.log(response);
      });
  }

  fetchDocumentConfig() {
    return this.http
    .get<DocumentConfig>(
        'https://notepad-a952b-default-rtdb.firebaseio.com/document-config.json'
    )
    .pipe(
        tap(documentConfig => {
          if(documentConfig == null) {
            this.documentService.setDocumentConfig(null);
          } else {
            this.documentService.setDocumentConfig(documentConfig);
          }
        })
    );
  }

  storeDocuments() {
    const documents = this.documentService.getDocuments();
    this.http
      .put(
        'https://notepad-a952b-default-rtdb.firebaseio.com/documents.json',
        documents
      )
      .subscribe(response => {
        console.log(response);
      });

  }

  fetchDocuments() {
        return this.http
        .get<Document[]>(
            'https://notepad-a952b-default-rtdb.firebaseio.com/documents.json'
        )
        .pipe(
            tap(documents => {

              if(documents == null) {
                this.documentService.setDocuments([]);
              } else {
                this.documentService.setDocuments(documents);
              }
            })
        );
    }   
}