import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { AuthErrorDialog } from './auth-error-dialog/auth-error-dialog.component';

@NgModule({
  declarations: [  
    LoginFormComponent,
    SignUpFormComponent,
    AuthErrorDialog
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([  
        {path: 'sign-up', component: SignUpFormComponent },
        {path: 'login', component: LoginFormComponent },
    ]),
    SharedModule
  ],
  entryComponents: [
    AuthErrorDialog
  ]
})
export class AuthModule {}
