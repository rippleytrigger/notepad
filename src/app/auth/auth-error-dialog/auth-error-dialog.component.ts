import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

export interface DialogData {
    message: string;
}  

@Component({
    selector: 'auth-error-dialog',
    templateUrl: 'auth-error-dialog.component.html',
})

export class AuthErrorDialog {
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        public dialogRef: MatDialogRef<AuthErrorDialog>,
    ) {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}