import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { DataStorageService } from '../shared/data-storage.service';
import { DocumentConfig } from './document-config/document-config.model';
import { DocumentService } from './documents.service';


@Injectable({
  providedIn: 'root'
})
export class DocumentConfigResolverService implements Resolve<DocumentConfig> {
  constructor(
    private dataStorageService: DataStorageService,
    private DocumentsService: DocumentService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const documentConfig = this.DocumentsService.getDocumentConfig();

    if (documentConfig == null || documentConfig == undefined) {
      return this.dataStorageService.fetchDocumentConfig();
    } else {
      return documentConfig;
    }
  }
}