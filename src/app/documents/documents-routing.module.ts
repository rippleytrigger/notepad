import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { DocumentConfigGuard } from './document-config.guard';
import { DocumentConfigResolverService } from './document-config.resolver.service';
import { DocumentConfigComponent } from './document-config/document-config.component';
import { DocumentDetailComponent } from './document-detail/document-detail.component';
import { DocumentEditComponent } from './document-edit/document-edit.component';
import { DocumentItemComponent } from './document-list/document-item/document-item.component';
import { DocumentListComponent } from './document-list/document-list.component';
import { DocumentsResolverService } from './documents-resolver.service';
import { DocumentsComponent } from './documents.component';

const routes: Routes = [
    {
        path: '',
        component: DocumentsComponent,
        children: [
            {
                path: '',
                component: DocumentListComponent,
                canActivate: [AuthGuard],
                resolve: [DocumentsResolverService, DocumentConfigResolverService],
                data: { breadcrumb: {alias: 'Documents'} },
                children: [
                    { 
                        path: 'new', component: DocumentEditComponent 
                    }, 
                    { 
                        path: 'config', component: DocumentConfigComponent 
                    }, 
                    {
                        path: ':id',
                        component: DocumentItemComponent,
                        resolve: [DocumentsResolverService, DocumentConfigResolverService],
                        data: { breadcrumb: {alias: 'DocumentItem'} },
                        children: [
                            {
                                path: '',
                                component: DocumentDetailComponent,
                                resolve: [DocumentsResolverService, DocumentConfigResolverService],
                                data: { breadcrumb: {alias: 'DocumentDetail'} },
                            },
                            {
                                path: 'edit',
                                component: DocumentEditComponent,
                                resolve: [DocumentsResolverService, DocumentConfigResolverService],
                                data: { breadcrumb: {alias: 'DocumentEdit'} }
                            },  
                        ],
                    }, 
                ]
            },
        ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class DocumentsRoutingModule {}
  