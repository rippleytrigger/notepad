export class Document {
    public title: string;
    public text: string;
    public color: string;
    public cols: number;
    public rows: number;

    constructor(title: string, text: string, color: string, cols: number, rows: number) {
        this.title = title;
        this.text = text;
        this.color = color;
        this.cols = cols;
        this.rows = rows;
    }
  }
  