import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DataStorageService } from 'src/app/shared/data-storage.service';
import { BreadcrumbService } from 'xng-breadcrumb';
import { Document } from '../documents.model';
import { DocumentService } from '../documents.service';

@Component({
  selector: 'app-document-detail',
  templateUrl: './document-detail.component.html',
  styleUrls: ['./document-detail.component.scss']
})
export class DocumentDetailComponent implements OnInit {
  document: Document;
  id: number;

  constructor(private documentService: DocumentService,
              private route: ActivatedRoute,
              private router: Router,
              private breadcrumbService: BreadcrumbService,
              private dataStorageService: DataStorageService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.document = this.documentService.getDocument(this.id);
      }
    )

    this.breadcrumbService.set('@DocumentDetails', 'Document Number ' + this.id)
  }

  onEditDocument() {
    this.router.navigate(['/documents/', this.id, 'edit']);
  }

  openDeleteDocumentDialog() {
    const dialogRef = this.dialog.open(DocumentDeleteDialog, {
      width: '250px',
    })

    dialogRef.afterClosed().subscribe(result => {     
      if(result == true){
        this.documentService.deleteDocument(this.id);
        this.dataStorageService.storeDocuments();
        this.router.navigate(['/documents'])
      }
    });
  }
}


@Component({
  selector: 'document-delete-dialog',
  templateUrl: 'document-delete-dialog.html',
})
export class DocumentDeleteDialog {
  constructor(
    public dialogRef: MatDialogRef<DocumentDeleteDialog>,
    public documentService: DocumentService,
    public dataStorageService: DataStorageService,
    public router: Router,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
