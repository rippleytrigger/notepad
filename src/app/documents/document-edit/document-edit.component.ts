import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DataStorageService } from 'src/app/shared/data-storage.service';
import { DocumentService } from '../documents.service';

@Component({
  selector: 'app-document-edit',
  templateUrl: './document-edit.component.html',
  styleUrls: ['./document-edit.component.scss']
})
export class DocumentEditComponent implements OnInit {
  componentTitle: string;
  id: number;
  editMode = false;
  editDocumentform: FormGroup;
  public options: Object;

  editorConfig: AngularEditorConfig;

  constructor(private route: ActivatedRoute,
    private documentService: DocumentService,
    private dataStorageService: DataStorageService,
    private router: Router
) { }

  ngOnInit(): void {
    let parentID = this.route.parent.snapshot.paramMap.get( "id" );

    this.id = +parentID;
    this.editMode = parentID != null;

    this.editorConfig = {
      editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '0',
        maxHeight: 'auto',
        width: 'auto',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Enter text here...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
          {class: 'arial', name: 'Arial'},
          {class: 'times-new-roman', name: 'Times New Roman'},
          {class: 'calibri', name: 'Calibri'},
          {class: 'comic-sans-ms', name: 'Comic Sans MS'}
        ],
        customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      uploadUrl: 'v1/image',
    }
    
    this.initForm();

    this.options = {
      'moreText': {

        'buttons': ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', 'textColor', 'backgroundColor', 'inlineClass', 'inlineStyle', 'clearFormatting']
    
      },
    
      'moreParagraph': {
    
        'buttons': ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'paragraphFormat', 'paragraphStyle', 'lineHeight', 'outdent', 'indent', 'quote']
    
      },
    
      'moreRich': {
    
        'buttons': ['insertLink', 'insertImage', 'insertVideo', 'insertTable', 'emoticons', 'fontAwesome', 'specialCharacters', 'embedly', 'insertFile', 'insertHR']
    
      },
    
      'moreMisc': {
    
        'buttons': ['undo', 'redo', 'fullscreen', 'print', 'getPDF', 'spellChecker', 'selectAll', 'html', 'help'],
    
        'align': 'right',
    
        'buttonsVisible': 2
    
      }
    }
  }

  onFormSubmit() {
    if (!this.editMode) {
      this.documentService.addDocument(this.editDocumentform.value)
    } else {
      this.documentService.updateDocument(this.id, this.editDocumentform.value)
    }

    this.dataStorageService.storeDocuments();

    this.onCancel();
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  initForm() {
    this.componentTitle = "New Document";
    let title = 'Put The Title Here';
    let text = 'Put Some Text Here';
    let color = '#000000';
    let cols = 0;
    let rows = 0;

    if (this.editMode) {
      this.componentTitle = "Edit Document";
      const document = this.documentService.getDocument(this.id);
      title = document.title;
      text = document.text;
      color = document.color;
      cols = document.cols;
      rows = document.rows;
    }

    this.editDocumentform = new FormGroup({
      title: new FormControl(title, [Validators.required, Validators.minLength(2)]),
      text: new FormControl(text, [ Validators.required ]),
      color: new FormControl(color, [Validators.required]),
      cols: new FormControl(cols, [Validators.required, Validators.min(1)]),
      rows: new FormControl(rows, [Validators.required, Validators.min(1)]),
    });  
  }

}

