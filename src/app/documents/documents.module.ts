import { DocumentsComponent } from "./documents.component";
import { DocumentDeleteDialog, DocumentDetailComponent } from "./document-detail/document-detail.component";

import { NgModule } from "@angular/core";
import { DocumentsRoutingModule } from "./documents-routing.module";
import { SharedModule } from "../shared/shared.module";
import { DocumentConfigDialog, DocumentListComponent } from './document-list/document-list.component';
import { DocumentEditComponent } from './document-edit/document-edit.component';
import { DocumentItemComponent } from './document-list/document-item/document-item.component';
import { DocumentConfigComponent } from './document-config/document-config.component';

@NgModule({
    declarations: [
        DocumentsComponent,
        DocumentDetailComponent,
        DocumentListComponent,
        DocumentEditComponent,
        DocumentItemComponent,
        DocumentDeleteDialog,
        DocumentConfigComponent,
        DocumentConfigDialog
    ],
    imports: [
        DocumentsRoutingModule,
        SharedModule
    ],
    providers: [],
    bootstrap: [],
    entryComponents: [
        DocumentDeleteDialog,
        DocumentConfigDialog
    ]
  })
  export class DocumentsModule { }
  