import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { DataStorageService } from '../shared/data-storage.service';
import { Document } from './documents.model';
import { DocumentService } from './documents.service';


@Injectable({
  providedIn: 'root'
})
export class DocumentsResolverService implements Resolve<Document[]> {
  constructor(
    private dataStorageService: DataStorageService,
    private DocumentsService: DocumentService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const documents = this.DocumentsService.getDocuments();

    if (documents.length === 0) {
      return this.dataStorageService.fetchDocuments();
    } else {
      return documents;
    }
  }
}


