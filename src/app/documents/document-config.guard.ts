import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router,
    UrlTree
  } from '@angular/router';
  import { Injectable } from '@angular/core';
  import { Observable } from 'rxjs';
  import { map, tap, take } from 'rxjs/operators';
import { DocumentService } from './documents.service';
  
 
  
  @Injectable({ providedIn: 'root' })
  export class DocumentConfigGuard implements CanActivate {
    constructor(private documentService: DocumentService, private router: Router) {}
  
    canActivate(
      route: ActivatedRouteSnapshot,
      router: RouterStateSnapshot
    ):
      | boolean
      | UrlTree
      | Promise<boolean | UrlTree>
      | Observable<boolean | UrlTree> {

        let documentConfig = this.documentService.getDocumentConfig();
        
        if(documentConfig == null || documentConfig == undefined) {
            return this.router.createUrlTree(['/documents', '/config']);
        }
      
        return true;
    }
  }