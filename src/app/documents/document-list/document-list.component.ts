import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DocumentConfig } from '../document-config/document-config.model';
import { Document } from '../documents.model';
import { DocumentService } from '../documents.service';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {
  documentConfig: DocumentConfig;
  documents: Document[];
  subscription: Subscription;
  documentConfigSubscription: Subscription;

  constructor(private documentsService: DocumentService,
            private router: Router,
            private route: ActivatedRoute,
            public dialog: MatDialog
            ) 
    { }

  ngOnInit(): void {
    this.subscription = this.documentsService.documentsChanged
      .subscribe(
        (documents: Document[]) => {
          this.documents = documents;
        }
      );
    this.documents = this.documentsService.getDocuments();

    this.documentConfigSubscription = this.documentsService.documentConfigChanged
        .subscribe(
          (documentConfig: DocumentConfig) => {
            this.documentConfig = documentConfig;
          }
        )
    this.documentConfig = this.documentsService.getDocumentConfig();

    if(this.documentConfig == null || this.documentConfig == undefined) {
      this.openDocumentConfigDialog();      
    }
  }

  openDocumentConfigDialog() {
    const dialogRef = this.dialog.open(DocumentConfigDialog, {
      width: '250px',
    }) 

    dialogRef.afterClosed().subscribe(result => {     
      this.router.navigate(['/documents/config']);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.documentConfigSubscription.unsubscribe();
  }
}

@Component({
  selector: 'document-config-dialog',
  templateUrl: 'document-config-dialog.html',
})
export class DocumentConfigDialog {
  constructor(
    public dialogRef: MatDialogRef<DocumentConfigDialog>,
    public router: Router,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
