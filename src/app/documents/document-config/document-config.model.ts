export class DocumentConfig {
    public cols: number;
    public rowHeight: string;
    public gutterSize: string;

    constructor(cols: number, rowHeight: string, gutterSize: string) {
        this.cols = cols;
        this.rowHeight = rowHeight;
        this.gutterSize = gutterSize;
    }
  }
  