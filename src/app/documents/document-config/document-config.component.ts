import { Component, KeyValueDiffers, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStorageService } from 'src/app/shared/data-storage.service';
import { DocumentService } from '../documents.service';
import { DocumentConfig } from './document-config.model';

@Component({
  selector: 'app-document-config',
  templateUrl: './document-config.component.html',
  styleUrls: ['./document-config.component.scss']
})
export class DocumentConfigComponent implements OnInit {
  documentConfig: DocumentConfig;
  editMode: boolean;
  documentConfigform: FormGroup;

  constructor(private documentService: DocumentService,
    private dataStorageService: DataStorageService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.documentConfig = this.documentService.getDocumentConfig();

    if(this.documentConfig != null) {
      this.editMode = true;
    } else {
      this.editMode = false;
    }

    this.initForm();
  }

  initForm() {
    let cols = 0;
    let rowHeight = '';
    let gutterSize = '';

    if(this.editMode) {
      cols = this.documentConfig.cols;
      rowHeight = this.documentConfig.rowHeight;
      gutterSize = this.documentConfig.gutterSize;
    }

    this.documentConfigform = new FormGroup({
      cols: new FormControl(cols, [Validators.required, Validators.min(1)]),
      rowHeight: new FormControl(rowHeight, [Validators.required, Validators.pattern(/\d+[p]{1}[x]{1}/)]),
      gutterSize: new FormControl(gutterSize, [Validators.required, Validators.pattern(/\d+[p]{1}[x]{1}/)]),
    });  
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onFormSubmit() {
    this.documentService.setDocumentConfig(this.documentConfigform.value);

    this.dataStorageService.storeDocumentConfig();

    this.onCancel();
  }

}
