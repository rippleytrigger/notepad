import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { DocumentConfig } from "./document-config/document-config.model";
import { Document } from "./documents.model";

@Injectable()
export class DocumentService{
    documentsChanged = new Subject<Document[]>();
    documentConfigChanged = new Subject<DocumentConfig>();

    private documents: Document[] = [];
    private documentConfig: DocumentConfig;
  
    constructor() {}

    setDocumentConfig(documentConfig: DocumentConfig){
      console.log(documentConfig);
      this.documentConfig = documentConfig;
      this.documentConfigChanged.next(this.documentConfig);
    }

    getDocumentConfig(){
      return this.documentConfig;
    }
  
    setDocuments(documents: Document[]) {
      this.documents = documents;
      this.documentsChanged.next(this.documents.slice());
    }
  
    getDocuments() {
      return this.documents.slice();
    }
  
    getDocument(index: number) {
      return this.documents[index];
    }
  
  
    addDocument(document: Document) {
      this.documents.push(document);
      this.documentsChanged.next(this.documents.slice());
    }
  
    updateDocument(index: number, newDocument: Document) {
      this.documents[index] = newDocument;
      this.documentsChanged.next(this.documents.slice());
    }
  
    deleteDocument(index: number) {
      this.documents.splice(index, 1);
      this.documentsChanged.next(this.documents.slice());
    }
  
}