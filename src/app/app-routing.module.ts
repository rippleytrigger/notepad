import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'documents', 
    loadChildren: () => import('./documents/documents.module').then(m => m.DocumentsModule), 
    data: {breadcrumb: { skip: true }}
  },
  {
    path: 'auth',
    loadChildren:  () => import('./auth/auth.module').then(m => m.AuthModule), 
    data: {breadcrumb: { skip: true }}
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
