import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BreadcrumbService } from "xng-breadcrumb";
import { AuthInterceptorService } from "./auth/auth-interceptor.service";
import { DocumentService } from "./documents/documents.service";


@NgModule({
    providers: [
        DocumentService,
        BreadcrumbService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        }      
    ],
  })
  export class CoreModule { }
  